/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zeytol.conexion;

import com.zeytol.clase.Mensajes;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.PropertyResourceBundle;

/**
 *
 * @author Aldomar
 */
public class Conexion {

    Mensajes mensajes = new Mensajes();
    private static final String properties = "Configurar.properties";
    PropertyResourceBundle file;
    public static Connection connection = null;
    public static String host, username, password, database, port;

    public Connection getConnection() {
        try {
            file = new PropertyResourceBundle(new FileInputStream(properties));
            host = file.getString("host");
            port = file.getString("port");
            username = file.getString("username");
            password = file.getString("password");
            database = file.getString("database");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" + port + ":" + database, username, password);
        } catch (IOException | ClassNotFoundException | SQLException e) {
            mensajes.mensajeError("Error en las credenciales del servidor.\n" + e);
            System.exit(0);
        }
        return connection;
    }

}
