/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zeytol.clase;

import com.zeytol.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aldomar
 */
public class Test {

    Conexion conexion = new Conexion();
    Mensajes mensajes = new Mensajes();

    public String conex() {
        String msj = "";
        Connection connection = conexion.getConnection();
        String sql = "select 'Bienvenidos a ZT' from dual";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            if (statement.execute()) {
                msj = "Bienvenidos a Zeytol";
            }
        } catch (SQLException ex) {
            mensajes.mensajeError("Error al ejecutar la consulta:\n" + ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                mensajes.mensajeError("Error al cerrar la conexión:\n" + ex);
            }
        }
        return msj;
    }
}
