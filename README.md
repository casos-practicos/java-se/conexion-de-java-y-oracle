#Conexión de Java y Oracle

En el caso práctico se realiza una conexión utilizando el lenguaje de programación 
Java y el motor de base de dato Oracle. Para su correcto funcionamiento se debe
configurar los datos correctos en el archivo Configurar.properties.

Se utilizó el IDE Netbeans v8.2 para su desarrollo.

Contacto: aomc@outlook.es / aomc06@gmail.com